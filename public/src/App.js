import './App.css';
import Header from "./MyComponents/Header";
import { Todos } from "./MyComponents/Todos";
import { Footer } from "./MyComponents/Footer";
import { AddTodo } from "./MyComponents/AddTodo";
import React, { useState } from 'react';
function App() {
  const addTodo = (title,desc) => {
    console.log("adding", title, desc)
    let sno = todos[todos.length-1].sno + 1;
    const myTodo ={
      sno: sno,
      title: title,
      desc: desc,
    }
    setTodos([...todos, myTodo]);
    console.log(myTodo);
  }
  const onDelete = (todo) => {
    console.log("i am delete", todo);
    setTodos(todos.filter((e) => {
      return e !== todo;

    }));
  }
  const [todos, setTodos] = useState([
    {
      sno: 1,
      title: "Ajay",
      desc: "Trainee"
    },
    {
      sno: 2,
      title: "Vikash",
      desc: "Software Engineer"
    },
    {
      sno: 3,
      title: "Rahul",
      desc: "Hr"
    }
  ]);
  return (
    <>
      <Header title="MyTodosList" searchBar={false} />
      <AddTodo addTodo={addTodo} />
      <Todos todos={todos} key={todos.sno} onDelete={onDelete} />
      <Footer />
    </>
  );
}

export default App;
